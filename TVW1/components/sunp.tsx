import { useState, useCallback } from "react";
import ReactFlow, {
  Controls,
  Background,
  applyNodeChanges,
  applyEdgeChanges,
  Panel,
} from "reactflow";
import "reactflow/dist/style.css";

const initialNodes = [
  {
    id: "1",
    data: { label: "12 Sunnah Prayers with Extras" },
    type: "input",
    position: { x: 0, y: 0 },
    style: { backgroundColor: "#379237", color: "#33FF00" },
  },
  {
    id: "2",
    data: { label: "Total(2) 2 Before Fajr" },
    position: { x: 300, y: 80 },
    style: { backgroundColor: "#1E5128", color: "#9CFF2E" },
  },
  {
    id: "3",
    data: { label: "Total(6) 4 Before Duhr & 2 After Duhr" },
    position: { x: 90, y: 190 },
    style: { backgroundColor: "#B3541E", color: "#FFD93D" },
  },
  {
    id: "4",
    data: { label: "Total(2) 2 After Magrib" },
    position: { x: -90, y: 195 },
    style: { backgroundColor: "#00337C", color: "#00FFFF" },
  },
  {
    id: "5",
    data: { label: "Total(2) 2 After Isha" },
    position: { x: -250, y: 190 },
    style: { backgroundColor: "#3F0071", color: "#E384FF" },
  },
  {
    id: "6",
    data: { label: "Total(4) 4 of Ishraq (Sunrise)" },
    position: { x: 290, y: 220 },
    style: { backgroundColor: "#C74B50", color: "#FFACAC" },
  },
  {
    id: "7",
    data: { label: "NoLIMIT - Tahajjud (Night Prayer)" },
    position: { x: -300, y: 90 },
    style: { backgroundColor: "#570A57", color: "#F73D93" },
  },
];

const initialEdges = [
  {
    id: "1-2",
    source: "1",
    target: "2",
    type: "bezier",
    animated: true,
    label: "Fajr",
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    labelStyle: { fill: "#33FF00", fontWeight: 700 },
    style: { stroke: "#33FF00" },
  },
  {
    id: "1-3",
    source: "1",
    target: "3",
    type: "bezier",
    animated: true,
    label: "Duhr",
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    labelStyle: { fill: "#FF6600", fontWeight: 700 },
    style: { stroke: "#FF6600" },
  },
  {
    id: "1-4",
    source: "1",
    target: "4",
    type: "bezier",
    animated: true,
    label: "Magrib",
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    labelStyle: { fill: "#00FFFF", fontWeight: 700 },
    style: { stroke: "#00FFFF" },
  },
  {
    id: "1-5",
    source: "1",
    target: "5",
    type: "bezier",
    animated: true,
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    label: "Isha",
    labelStyle: { fill: "#9900F0", fontWeight: 700 },
    style: { stroke: "#9900F0" },
  },
  {
    id: "1-6",
    source: "1",
    target: "6",
    type: "bezier",
    animated: true,
    label: "Ishraq",
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    labelStyle: { fill: "#FF7777", fontWeight: 700 },
    style: { stroke: "#FF7777" },
  },
  {
    id: "1-7",
    source: "1",
    target: "7",
    type: "bezier",
    animated: true,
    label: "Tahajjud",
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    labelStyle: { fill: "#FF00E4", fontWeight: 900 },
    style: { stroke: "#FF00E4" },
  },
];

function Flow() {
  const [nodes, setNodes] = useState(initialNodes);
  const [edges, setEdges] = useState(initialEdges);

  const onNodesChange = useCallback(
    (changes) => setNodes((nds) => applyNodeChanges(changes, nds)),
    []
  );
  const onEdgesChange = useCallback(
    (changes) => setEdges((eds) => applyEdgeChanges(changes, eds)),
    []
  );

  return (
    <div style={{ height: "60%" }}>
      <ReactFlow
        nodes={nodes}
        onNodesChange={onNodesChange}
        edges={edges}
        onEdgesChange={onEdgesChange}
        fitView
      >
        <Panel position="top-right">Sunnah Prayers and Extras</Panel>
        <Background />
        <Controls />
      </ReactFlow>
    </div>
  );
}

export default Flow;
