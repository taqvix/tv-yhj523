import Image from "next/image";
import Link from "next/link";
import fus from "../img/fus.png";

function headimage() {
  return (
    <Link
      href="https://youtube.com/playlist?list=PLYZxc42QNctWpaUoIWezGeaw6VN-5db1M"
      target="_blank"
    >
      <Image src={fus} width={320} height={180} alt="YouTube Playlist" />
    </Link>
  );
}

export default headimage;
