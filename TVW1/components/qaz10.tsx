import Image from "next/image";
import Link from "next/link";
import qaz from "../img/qaz10.png";

function headimage() {
  return (
    <Link href="https://quran.com/39/10?translations=20%2C171" target="_blank">
      <Image src={qaz} width={800} alt="AzZumarV10" />
    </Link>
  );
}

export default headimage;
