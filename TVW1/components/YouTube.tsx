// Deeds that build houses in janna 
// Yaquin Institute Jannah Series 
// https://youtu.be/IHVUXsMWkZI 
 
import React from "react";
import YouTube, { YouTubeProps } from "react-youtube";
import styles from "./YouTube.module.css";

function myvid() {
  const onPlayerReady: YouTubeProps["onReady"] = (event) => {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  };

  const opts: YouTubeProps["opts"] = {
    height: "180",
    width: "320",
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 0,
      loop: 1,
    },
  };

  return <YouTube videoId="IHVUXsMWkZI" opts={opts} onReady={onPlayerReady} />;
}

export default myvid;
