import Image from "next/image";
import Link from "next/link";
import dus2 from "../img/dus2.png";

function headimage() {
  return (
    <Link href="https://sunnah.com/hisn:111" target="_blank">
      <Image src={dus2} width={800} alt="AzZumarV10" />
    </Link>
  );
}

export default headimage;
