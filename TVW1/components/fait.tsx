import React, { PureComponent } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Bar,
  BarChart,
  Label,
} from "recharts";

// Muharram - MU
// Safar - SA
// Rabi al-Awwal - RA
// Rabi al-Thani - RT
// Jumada al-Awwal - JA
// Jumada al-Thani - JT
// Rajab - RA
// Shaban - SH
// Ramadan - RM
// Shawwal - SW
// Dhu al-Qadah - DQ
// Dhu al-Hijjah (month of Hajj) - DAH

const data = [
  {
    name: "1MH",
    faith: 70,
    amt: 1,
  },

  {
    name: "2SA",
    faith: 10,
    amt: 2,
  },

  {
    name: "3RA",
    faith: 20,
    amt: 3,
  },

  {
    name: "4RT",
    faith: 30,
    amt: 4,
  },

  {
    name: "5JA",
    faith: 40,
    amt: 5,
  },

  {
    name: "6JT",
    faith: 50,
    amt: 6,
  },

  {
    name: "7RJ",
    faith: 70,
    amt: 7,
  },

  {
    name: "8SH",
    faith: 80,
    amt: 8,
  },

  {
    name: "9RM",
    faith: 100,
    amt: 9,
  },

  {
    name: "10SW",
    faith: 80,
    amt: 10,
  },

  {
    name: "11DQ",
    faith: 40,
    amt: 11,
  },

  {
    name: "12DJ",
    faith: 60,
    amt: 12,
  },
];

function Lin_char() {
  return (
    <BarChart
      width={730}
      height={400}
      data={data}
      margin={{ top: 15, right: 30, left: 20, bottom: 15 }}
    >
      <CartesianGrid stroke="#064663" strokeDasharray="3 3" />
      <XAxis dataKey="name" stroke="#00FFAB">
        <Label
          value="Faith Level alteration across months"
          offset={0}
          stroke="#FF78F0"
          position="bottom"
        />
      </XAxis>
      <YAxis dataKey="faith" stroke="#00FFDD">
        <Label
          value="Faith Level as %"
          angle={-90}
          offset={10}
          stroke="#FF78F0"
          position="insideLeft"
        />
      </YAxis>
      <Tooltip />
      <Legend verticalAlign="top" height={36} />
      <Bar dataKey="faith" fill="#14C38E" />
    </BarChart>
  );
}

export default Lin_char;
