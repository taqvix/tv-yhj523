import { useState, useCallback } from "react";
import ReactFlow, {
  Controls,
  Background,
  applyNodeChanges,
  applyEdgeChanges,
  Panel,
} from "reactflow";
import "reactflow/dist/style.css";

const initialNodes = [
  {
    id: "1",
    data: { label: "Builder Deeds" },
    position: { x: 50, y: 0 },
    type: "input",
    style: { backgroundColor: "#379237", color: "#9CFF2E" },
  },
  {
    id: "2",
    data: { label: "Sabr(Patience) Best Ones" },
    position: { x: -150, y: 100 },
    style: { backgroundColor: "#38E54D", color: "#FDFF00" },
  },

  // Specific Good Deeeds Set

  {
    id: "3",
    data: { label: "Specific Good Deeds" },
    position: { x: 200, y: 80 },
    style: { backgroundColor: "#1E5128", color: "#FF78F0" },
  },
  {
    id: "31",
    data: { label: "Ibadah(Worship)" },
    position: { x: 300, y: 190 },
    style: { backgroundColor: "#3B185F", color: "#9CFF2E" },
  },
  {
    id: "311",
    data: { label: "1.Build Masjid" },
    position: { x: 510, y: 250 },
    style: { backgroundColor: "#3B185F", color: "#9CFF2E" },
  },
  {
    id: "312",
    data: { label: "2.Pray Extra 12 Extra Sunnah in Home" },
    position: { x: 520, y: 350 },
    style: { backgroundColor: "#3B185F", color: "#9CFF2E" },
  },
  {
    id: "313",
    data: { label: "3.Recite Ch112 10 Times in a day" },
    position: { x: 530, y: 450 },
    style: { backgroundColor: "#3B185F", color: "#9CFF2E" },
  },
  {
    id: "314",
    data: { label: "4.Dua Before Sleep" },
    position: { x: 540, y: 550 },
    style: { backgroundColor: "#3B185F", color: "#9CFF2E" },
  },
  {
    id: "315",
    data: { label: "5.Dhikr when entering a souq (online included)" },
    position: { x: 550, y: 650 },
    style: { backgroundColor: "#3B185F", color: "#9CFF2E" },
  },

  // Character Transformation Set Number 4

  {
    id: "4",
    data: { label: "Character Transformation via Worship" },
    position: { x: 50, y: 200 },
    style: { backgroundColor: "#7B113A", color: "#9CFF2E" },
  },
  {
    id: "41",
    data: { label: "1.Feeding the poor (Practice of Gratitude)" },
    position: { x: 10, y: 350 },
    style: { backgroundColor: "#7B113A", color: "#9CFF2E" },
  },
  {
    id: "42",
    data: {
      label: "2.Consistent Fasting including voluntary (Practice of Sabr)",
    },
    position: { x: -20, y: 450 },
    style: { backgroundColor: "#7B113A", color: "#9CFF2E" },
  },
  {
    id: "43",
    data: { label: "3.Praying at night when others sleep (Practice of Ihsan)" },
    position: { x: -80, y: 550 },
    style: { backgroundColor: "#7B113A", color: "#9CFF2E" },
  },
  {
    id: "44",
    data: {
      label:
        "4.Giving up arguing even when they are right (Practice of forbearance )",
    },
    position: { x: -150, y: 650 },
    style: { backgroundColor: "#7B113A", color: "#9CFF2E" },
  },
  {
    id: "45",
    data: {
      label: "5.Avoids lying even when joking  (Practice of Cautious)",
    },
    position: { x: -250, y: 750 },
    style: { backgroundColor: "#7B113A", color: "#9CFF2E" },
  },
];

const initialEdges = [
  {
    id: "1-2",
    source: "1",
    target: "2",
    type: "smoothstep",
    animated: true,
    label: "Genre 1",
    labelStyle: { fill: "#16FF00", fontWeight: 700 },
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    style: { stroke: "#16FF00" },
  },
  {
    id: "1-3",
    source: "1",
    target: "3",
    type: "smoothstep",
    animated: true,
    label: "Genre 2",
    labelBgBorderRadius: 4,
    labelStyle: { fill: "#FF78F0", fontWeight: 700 },
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    style: { stroke: "#FF78F0" },
  },
  {
    id: "3-1",
    source: "3",
    target: "31",
    type: "smoothstep",
    animated: true,
    label: "Worship",
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    labelStyle: { fill: "#A31ACB", fontWeight: 700 },
    style: { stroke: "#A31ACB" },
  },

  // Specific Good Deeds Set

  {
    id: "31-311",
    source: "31",
    target: "311",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#E90064", fontWeight: 700 },
    style: { stroke: "#E90064" },
  },
  {
    id: "31-312",
    source: "31",
    target: "312",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#E90064", fontWeight: 700 },
    style: { stroke: "#E90064" },
  },
  {
    id: "31-313",
    source: "31",
    target: "313",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#E90064", fontWeight: 700 },
    style: { stroke: "#E90064" },
  },
  {
    id: "31-314",
    source: "31",
    target: "314",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#E90064", fontWeight: 700 },
    style: { stroke: "#E90064" },
  },
  {
    id: "31-315",
    source: "31",
    target: "315",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#E90064", fontWeight: 700 },
    style: { stroke: "#E90064" },
  },

  // Character related number 4

  {
    id: "3-4",
    source: "3",
    target: "4",
    type: "smoothstep",
    animated: true,
    label: "Character Building",
    labelBgBorderRadius: 4,
    labelBgStyle: { fill: "#000000", fillOpacity: 0.7 },
    labelStyle: { fill: "#EA047E", fontWeight: 700 },
    style: { stroke: "#EA047E" },
  },
  {
    id: "4-41",
    source: "4",
    target: "41",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#EA047E", fontWeight: 700 },
    style: { stroke: "#EA047E" },
  },
  {
    id: "4-42",
    source: "4",
    target: "42",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#EA047E", fontWeight: 700 },
    style: { stroke: "#EA047E" },
  },
  {
    id: "4-43",
    source: "4",
    target: "43",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#EA047E", fontWeight: 700 },
    style: { stroke: "#EA047E" },
  },
  {
    id: "4-44",
    source: "4",
    target: "44",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#EA047E", fontWeight: 700 },
    style: { stroke: "#EA047E" },
  },
  {
    id: "4-45",
    source: "4",
    target: "45",
    type: "smoothstep",
    animated: true,
    labelStyle: { fill: "#EA047E", fontWeight: 700 },
    style: { stroke: "#EA047E" },
  },
];

function Flow() {
  const [nodes, setNodes] = useState(initialNodes);
  const [edges, setEdges] = useState(initialEdges);

  const onNodesChange = useCallback(
    (changes) => setNodes((nds) => applyNodeChanges(changes, nds)),
    []
  );
  const onEdgesChange = useCallback(
    (changes) => setEdges((eds) => applyEdgeChanges(changes, eds)),
    []
  );

  return (
    <div style={{ height: "60%" }}>
      <ReactFlow
        nodes={nodes}
        onNodesChange={onNodesChange}
        edges={edges}
        onEdgesChange={onEdgesChange}
        fitView
      >
        <Panel position="top-right">
          Overview of Deeds that lead to Paradise
        </Panel>
        <Background />
        <Controls />
      </ReactFlow>
    </div>
  );
}

export default Flow;
