import Image from 'next/image'
import Link from 'next/link'
import b1h from '../img/b1.png'

function headimage() {
    return (
        <Image 
        src={b1h}
        width={600}
        alt="Bismillah"
        />
    )
}

export default headimage