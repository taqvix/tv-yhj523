import Image from 'next/image'
import Link from 'next/link'
import mysu from '../img/mysu.jpg'

function headimage() {
    return (
        <Image 
        src={mysu}
        width={600}
        alt="PrayerTable"
        />
    )
}

export default headimage