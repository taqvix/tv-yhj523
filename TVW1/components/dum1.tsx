import Image from "next/image";
import Link from "next/link";
import dum1 from "../img/dusm1.png";

function headimage() {
  return (
    <Link href="https://sunnah.com/hisn:209" target="_blank">
      <Image src={dum1} width={800} alt="AzZumarV10" />
    </Link>
  );
}

export default headimage;
