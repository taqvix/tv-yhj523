import Image from "next/image";
import Link from "next/link";
import ytp from "../img/ytpl.png";

function myimaga() {
  return (
    <Link
      href="https://youtube.com/playlist?list=PLQ02IYL5pmhGLpO-oUMpZbuI_5dT9m4fi"
      target="_blank"
    >
      <Image src={ytp} width={320} height={180} alt="YouTube Playlist" />
    </Link>
  );
}

export default myimaga;
