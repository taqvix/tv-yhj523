import Image from "next/image";
import Link from "next/link";
import dus from "../img/dus.png";

function headimage() {
  return (
    <Link href="https://sunnah.com/tirmidhi:3574" target="_blank">
      <Image src={dus} width={800} alt="AzZumarV10" />
    </Link>
  );
}

export default headimage;
