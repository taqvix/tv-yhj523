import React from "react";
import { DocsThemeConfig } from "nextra-theme-docs";
import { useRouter } from "next/router";

const config: DocsThemeConfig = {
  head: (
    <>
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href="/favicon-32x32.png"
      ></link>
      <link
        rel="icon"
        type="image/png"
        sizes="96x96"
        href="/favicon-96x96.png"
      ></link>
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href="/favicon-16x16.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="57x57"
        href="/apple-icon-57x57.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="60x60"
        href="/apple-icon-60x60.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="72x72"
        href="/apple-icon-72x72.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="76x76"
        href="/apple-icon-76x76.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="114x114"
        href="/apple-icon-114x114.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="120x120"
        href="/apple-icon-120x120.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="144x144"
        href="/apple-icon-144x144.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="152x152"
        href="/apple-icon-152x152.png"
      ></link>
      <link
        rel="apple-touch-icon"
        sizes="180x180"
        href="/apple-icon-180x180.png"
      ></link>
      <link
        rel="icon"
        type="image/png"
        sizes="192x192"
        href="/android-icon-192x192.png"
      ></link>
      <link rel="manifest" href="/manifest.json"></link>
      <meta name="theme-color" content="#ffffff"></meta>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta property="og:title" content="DuasExtract" />
      <meta
        property="og:description"
        content="Extracted duas from youtube video"
      />
      <meta
        name="description"
        content="Extracted duas from youtube video"
      ></meta>

      <meta property="og:url" content="https://tvjw.vercel.app/"></meta>
      <meta property="og:type" content="website"></meta>
      <meta property="og:title" content="DuasExtract"></meta>
      <meta
        property="og:description"
        content="Extracted duas from youtube video"
      ></meta>
      <meta
        property="og:image"
        content="https://i.ibb.co/YBMVFvH/image.png"
      ></meta>

      <meta name="twitter:card" content="summary_large_image"></meta>
      <meta property="twitter:domain" content="tvjw.vercel.app"></meta>
      <meta property="twitter:url" content="https://tvjw.vercel.app/"></meta>
      <meta name="twitter:title" content="DuasExtract"></meta>
      <meta
        name="twitter:description"
        content="Extracted duas from youtube video"
      ></meta>
      <meta
        name="twitter:image"
        content="https://i.ibb.co/YBMVFvH/image.png"
      ></meta>
      {/* <title>🕌</title> */}
    </>
  ),
  project: {
    link: "https://gitlab.com/taqvix/tv-yhj523",
  },
  docsRepositoryBase: "https://gitlab.com/taqvix/tv-yhj523",
  footer: {
    text: (
      <span>
        <a href="https://youtu.be/IHVUXsMWkZI" target="_blank">
          "Duas Extracted from -10 Deeds That Build Houses in Jannah | Ep. 6 |
          #JannahSeries with Dr. Omar Suleiman"
        </a>
      </span>
    ),
  },
  primaryHue: { dark: 137, light: 225 },
  logo: (
    <>
      <svg width="24" height="24" viewBox="0 0 34 34">
        <path
          fill="currentColor"
          d="M 18.5 3 C 13.806 3 10 6.806 10 11.5 C 10 12.542294 10.19765 13.536204 10.541016 14.458984 L 3 22 L 3 27 L 8 27 L 8 24 L 11 24 L 11 21 L 14 21 L 15.541016 19.458984 C 16.463796 19.80235 17.457706 20 18.5 20 C 23.194 20 27 16.194 27 11.5 C 27 6.806 23.194 3 18.5 3 z M 20.5 7 C 21.881 7 23 8.119 23 9.5 C 23 10.881 21.881 12 20.5 12 C 19.119 12 18 10.881 18 9.5 C 18 8.119 19.119 7 20.5 7 z"
        />
      </svg>
      <span style={{ marginLeft: ".4em", fontWeight: 800 }}>DuasExtract</span>
    </>
  ),
  useNextSeoProps() {
    return {
      titleTemplate: "%s - 🕌",
    };
  },
  darkMode: true,
};

export default config;
