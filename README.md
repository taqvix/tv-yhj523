<img src="https://gitlab.com/ddokk/gfx/-/raw/main/g/b1.png" width=400>

---

<h1 align="center"><code> 🕌: Duas Extract </code></h1>
<h2 align="center"><i> Youtube Video Study  </i></h2>

---

1. [What ?](#what-)
2. [Local Development Instructions](#local-development-instructions)
3. [Warning](#warning)
4. [Repo Authenticity](#repo-authenticity)
   1. [OnChain Verification Message](#onchain-verification-message)
   2. [Onchain TX](#onchain-tx)
   3. [Repo Image](#repo-image)

---

# What ?

![Vid](./TVW1/DP.mp4)

[🤖Jump to site](https://cutt.ly/tvyw)

# Local Development Instructions

_Clone Repo_

```sh
git clone https://gitlab.com/taqvix/tv-yhj523
```

_Change into directory and install dependencies_

```sh
cd tv-yhj523
pnpm config set auto-install-peers true
pnpm i
```

Run Dev Server

```sh
pnpm dev
```

# Warning

<img src="https://media.tenor.com/tbb9HyHSIdQAAAAC/warning-sign.gif" width=200>

$$
\mathrm{}\textcolor{red}{\colorbox{black}{\textbf{No information accuracy guarantee. Usaga at own risk. }}}
$$

# Repo Authenticity 

<details>

<summary>
`Website Repo Authenticity Details`
</summary>

## OnChain Verification Message 

```ml 
https://gitlab.com/taqvix/tv-yhj523
Deeds of Janah Dua Extract 

********************************************************************************

43:72
وَتِلْكَ ٱلْجَنَّةُ ٱلَّتِىٓ أُورِثْتُمُوهَا بِمَا كُنتُمْ تَعْمَلُونَ ٧٢

That is the Paradise which you will be awarded for what you used to do.


********************************************************************************

```

## Onchain TX 

N | Hash
|:--:|:--:|
[`Sepolia`](https://sepolia.etherscan.io/tx/0x12717379a9ec7cbc917fff52c2b64f6110809c9e0b2f05656d57fec78831a345) | [`0x12717379a9ec7cbc917fff52c2b64f6110809c9e0b2f05656d57fec78831a345`](https://sepolia.etherscan.io/tx/0x12717379a9ec7cbc917fff52c2b64f6110809c9e0b2f05656d57fec78831a345)
[`Mumbai`](https://mumbai.polygonscan.com/tx/0x2e24d27f111e93314cd52258a72c2e11d0e36da05790cfb7af0596dda187ee36) | [`0x2e24d27f111e93314cd52258a72c2e11d0e36da05790cfb7af0596dda187ee36`](https://mumbai.polygonscan.com/tx/0x2e24d27f111e93314cd52258a72c2e11d0e36da05790cfb7af0596dda187ee36)
[`BinancTestNet`](https://testnet.bscscan.com/tx/0x387b4617250bfd9da08ca53fa1d81abe6ad2f5548681034cb203c10aba0775e4) | [`0x387b4617250bfd9da08ca53fa1d81abe6ad2f5548681034cb203c10aba0775e4`](https://testnet.bscscan.com/tx/0x387b4617250bfd9da08ca53fa1d81abe6ad2f5548681034cb203c10aba0775e4)

## Repo Image 

N | Sha512 
|:--:|:--:|
![](TVW1/output.png) | [`sha512`](./TVW1/sh512repo.txt)


</details>
